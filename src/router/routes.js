import Login from '../views/login/Login.vue';
import NotFound from "@/views/notFoundPage/NotFoundPage.vue";

const Dashboard = () => import(/* webpackChunkName: "dashboard" */"@/views/dashboard/Dashboard.vue");
const ClientList = () => import(/* webpackChunkName: "clients" */ "@/views/client/ClientList.vue");
const ClientForm = () => import(/* webpackChunkName: "clientform" */ "@/views/client/ClientForm.vue");

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { guest: true },
  },
  {
    path: "/",
    //component: DashboardLayout,
    component: () => import('@/layout/dashboard/MainContainer.vue'),
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
        meta: {requiresAuth: true},
      },
      {
        path: "clientlist",
        name: "clientlist",
        component: ClientList,
        meta: {requiresAuth: true},
      },
      {
        path: 'clientlist/:id',
        name: "ClientUpdate",
        component: ClientForm,
        meta: {requiresAuth: true},
        props: true
      },
      {
        path: 'clientlist/new',
        name: "ClientForm",
        component: ClientForm,
        meta: {requiresAuth: true},
        props: true
      }
    ]
  },
  
  { path: "/404",alias: "*", component: NotFound },
]


export default routes;
