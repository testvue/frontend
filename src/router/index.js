
import VueRouter from 'vue-router'
import routes from "./routes";
// import store from "../store/store";

const router = new VueRouter({
  routes,
  mode: 'history',
  base: process.env.BASE_URL,
})

router.beforeEach((to, from, next) => {
  
  console.log('isAuthenticated : ', (localStorage.getItem('token')) )
  if(to.matched.some(record => record.meta.requiresAuth)) {
    const isAuthenticated  = localStorage.getItem('token')?true:false; 
    console.log(isAuthenticated)
    if (isAuthenticated) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    const isAuthenticated  = localStorage.getItem('token')?true:false; 
    console.log(isAuthenticated)
    if (isAuthenticated) {
      next("/dashboard");
      return;
    }
    next();
  } else {
    next();
  }
});



export default router
