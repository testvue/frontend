import axios from 'axios'
//import store from './store/store'
import router from './router'
//import authHeader from './service/auth-header';
const baseURL = 'http://localhost:49817/api/'

//axios.defaults.withCredentials = true
const config = {
  baseURL: baseURL,
  headers: {
    'Content-Type': 'application/json',
    'auth_token' : ''
  },
  //timeout: 10000,
  params: {}
}

const instance = axios.create(config);

instance.interceptors.response.use(undefined, function (error) {
  if (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
  
        originalRequest._retry = true;
        //store.dispatch('LogOut')
        return router.push('/login')
    }
  }
})

instance.interceptors.request.use(function (config) {
  //const token = this.$store.token;
  config.headers.auth_token =  localStorage.getItem('token')
  
  return config;
});


export default instance