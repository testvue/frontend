import axios from '../../axios.js'

export default {
//   getHelloWord({commit}){
//       commit('HELLO','WORKING')
//   }
    loginRequest({commit},item) {
        return new Promise((resolve,reject)=>{
            axios.post('user/login',{
                email: item.email,
                password: item.password,
                gethash: true
            }).then((response) => {
                commit('LOGIN',response.data);
                if(response.data.state){
                    resolve(response);
                }else{
                    reject(new Error(response.data.message))
                    //throw new Error(response.data.message);
                }
                
            }).catch(err => {
                if(err.response){
                    commit('ERROR',err.response.data);
                }else{
                    commit('ERROR',err);
                }
                
                reject(new Error(err.message));
            });
        })
    },
    signupRequest({commit},item) {
        return new Promise((resolve)=>{
            axios.post('user/register',{
                name: item.name,
                email: item.email,
                password: item.password,
                
            }).then(response => {
                commit('REGISTER',response.data);
                resolve(response);
            });
        })
    }

}