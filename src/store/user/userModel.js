import actions from './userActions.js';
import state from './userState.js';
import mutations from './userMutations.js';

export default {
    isRegister: false,
    namespaced: true,
    actions,
    state,
    mutations
}