export default {
    LOGIN (state, item){
        state.user = item.data;
        state.token = item.token;
        state.apiState = item.state;
        state.apiMessage = item.message;
    },
    REGISTER (state, item){
        state.user = item.data;
        state.token = item.token;
        state.apiState = item.state;
        state.apiMessage = item.message;
        this.$store.commit('ACTIVE_USER', state.user);

    },
    ERROR(state,item){
        state.apiState = item.state?item.state:false;
        state.apiMessage = item.message;
    },
    HELLO(state, msg){
        state.carlos = msg;
    }
    

}