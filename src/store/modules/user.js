
//import LoginService from '../services/login.service';
// initial state
const state = () => ({
  all: []
})

// getters
const getters = {}

// actions
const actions = {
  logIn () {
  }
}

// mutations
const mutations = {
  
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
