

import axios from '../../axios.js'
const state = {
    user: null,
    token:null,
    posts: null,
};
const getters = {
    isAuthenticated(state){
        return !!state.user;
    },
    //isAuthenticated: state => !!state.user,    
    StatePosts: state => state.posts,
    StateUser: state => state.user,
    StateToken: state => state.token,
};
const actions = {
    async Register({dispatch}, form) {
        await axios.post('register', form)
        let UserForm = new FormData()
        UserForm.append('username', form.username)
        UserForm.append('password', form.password)
        await dispatch('LogIn', UserForm)
      },
      async LogIn({commit}, User) {
        await axios.post('login', User)
        await commit('setUser', User.get('username'))
      },
      async CreatePost({dispatch}, post) {
        await axios.post('post', post)
        await dispatch('GetPosts')
      },
      async GetPosts({ commit }){
        let response = await axios.get('posts')
        commit('setPosts', response.data)
      },
      async LogOut({commit}){
        let user = null
        localStorage.removeItem('token')
        commit('LogOut', user)
      }
};
const mutations = {
    setUser(state, username){
        state.user = username
    },
    setToken(state, token){
      localStorage.setItem('token',token)
      state.token = token
    },
    setPosts(state, posts){
        state.posts = posts
    },
    LogOut(state){
      localStorage.removeItem('token');
      console.log('logout');
        state.user = null;
        state.posts = null;
        state.token = null;
    },
};
export default {
  state,
  getters,
  actions,
  mutations
};
