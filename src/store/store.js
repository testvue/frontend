import Vuex from 'vuex';
import Vue from 'vue';
import createPersistedState from "vuex-persistedstate";
import auth from './modules/auth';

Vue.use( Vuex);

const store = new Vuex.Store ({
    modules: {
        auth
      },
    state: {
    
        isSidebarActive: false,
        themeColor: "#2962ff",
        activeUser:[],
       // token:'test'
    },
    mutations: {
        //This is for Sidbar trigger in mobile
        IS_SIDEBAR_ACTIVE(state, value) {
            state.isSidebarActive = value;
        },
        ACTIVE_USER(state, value) {
            state.activeUser = value;
        },
    },  
    actions: {
        
    },
    getters:{
        
    },
    plugins: [createPersistedState()]
});

export default store;

