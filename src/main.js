import Vue from 'vue'
import vuesax from 'vuesax';

import App from './App.vue'
import 'vuesax/dist/vuesax.css'
import 'material-icons/iconfont/material-icons.css';

import store from './store/store'
import VueRouter from "vue-router";
import router from './router'
import vuetify from './plugins/vuetify';
import VueClipboard from 'vue-clipboard2'
import vPlayBack from 'v-playback';

import axios from './axios';

Vue.config.productionTip = false
Vue.config.devtools = true
Vue.config.productionTip = false

Vue.use(vuesax);
Vue.use(VueRouter);
Vue.use(VueClipboard);
Vue.use(vPlayBack);
const v = new Vue({
  router,
  vuetify,
  axios,
  store,
  render: h => h(App)
}).$mount('#app');
console.log(v);
import './assets/scss/style.scss'